package de.fuchspfoten.rohrpost.module;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.data.PlayerData;
import de.fuchspfoten.rohrpost.RohrpostPlugin;
import de.fuchspfoten.rohrpost.TeleportTask;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * /tpa &lt;player&gt; requests to teleport to a player.
 * /tptoggle toggles teleport requests from other players.
 * /tpaccept accepts a teleport request.
 */
public class RequestTeleportModule implements CommandExecutor {

    /**
     * The parser for command arguments for /tpa.
     */
    private final ArgumentParser argumentParserTPA;

    /**
     * The parser for command arguments for /tptoggle.
     */
    private final ArgumentParser argumentParserTPToggle;

    /**
     * The parser for command arguments for /tpaccept.
     */
    private final ArgumentParser argumentParserTPAccept;

    /**
     * UUIDs which currently are on cooldown for new requests.
     */
    private final Set<UUID> requestCooldown = new HashSet<>();

    /**
     * Maps requested UUIDs to requesting UUIDs.
     */
    private final Map<UUID, UUID> teleportRequests = new HashMap<>();

    /**
     * Constructor.
     */
    public RequestTeleportModule() {
        Messenger.register("rohrpost.tpa.toggleOff");
        Messenger.register("rohrpost.tpa.toggleOn");
        Messenger.register("rohrpost.tpa.noRequest");
        Messenger.register("rohrpost.tpa.accepted");
        Messenger.register("rohrpost.tpa.noTarget");
        Messenger.register("rohrpost.tpa.cooldown");
        Messenger.register("rohrpost.tpa.sent");
        Messenger.register("rohrpost.tpa.received");
        Messenger.register("rohrpost.tpa.isOff");

        argumentParserTPA = new ArgumentParser();
        argumentParserTPToggle = new ArgumentParser();
        argumentParserTPAccept = new ArgumentParser();

        Bukkit.getPluginCommand("tpa").setExecutor(this);
        Bukkit.getPluginCommand("tptoggle").setExecutor(this);
        Bukkit.getPluginCommand("tpaccept").setExecutor(this);
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        // Player check.
        if (!(sender instanceof Player)) {
            sender.sendMessage("player-only!");
            return true;
        }
        final Player player = (Player) sender;

        // Permission check.
        if (!sender.hasPermission("rohrpost.tpa")) {
            sender.sendMessage("Missing permission: rohrpost.tpa");
            return true;
        }

        if (command.getName().equals("tpa")) {
            // Syntax check.
            args = argumentParserTPA.parse(args);
            if (args.length != 1 || argumentParserTPA.hasArgument('h')) {
                Messenger.send(sender, "commandHelp", "Syntax: /tpa <player>");
                argumentParserTPA.showHelp(sender);
                return true;
            }

            // Check the cooldown.
            if (requestCooldown.contains(player.getUniqueId())) {
                Messenger.send(sender, "rohrpost.tpa.cooldown");
                return true;
            }

            // Fetch the target.
            final Player target = Bukkit.getPlayer(args[0]);
            if (target == null || !target.isOnline() || !player.canSee(target)) {
                Messenger.send(sender, "rohrpost.tpa.noTarget");
                return true;
            }

            // Disallow teleport to self.
            if (target == sender) {
                sender.sendMessage("invalid target");
                return true;
            }

            // Check the target's data.
            final PlayerData pd = new PlayerData(target);
            if (pd.hasFlag("rohrpost.tptoggle")) {
                Messenger.send(sender, "rohrpost.tpa.isOff");
                return true;
            }

            // Set a cooldown.
            final UUID senderId = player.getUniqueId();
            requestCooldown.add(senderId);
            RohrpostPlugin.scheduleTask(() -> requestCooldown.remove(senderId), 10 * 20L);

            // Request the teleport.
            teleportRequests.put(target.getUniqueId(), senderId);
            Messenger.send(sender, "rohrpost.tpa.sent");
            Messenger.send(target, "rohrpost.tpa.received", sender.getName());
        } else if (command.getName().equals("tptoggle")) {
            // Syntax check.
            args = argumentParserTPToggle.parse(args);
            if (args.length != 0 || argumentParserTPToggle.hasArgument('h')) {
                Messenger.send(sender, "commandHelp", "Syntax: /tptoggle");
                argumentParserTPToggle.showHelp(sender);
                return true;
            }

            // Toggle teleport requests.
            final PlayerData pd = new PlayerData((OfflinePlayer) sender);
            pd.toggleFlag("rohrpost.tptoggle");
            if (pd.hasFlag("rohrpost.tptoggle")) {
                Messenger.send(sender, "rohrpost.tpa.toggleOff");
            } else {
                Messenger.send(sender, "rohrpost.tpa.toggleOn");
            }
        } else if (command.getName().equals("tpaccept")) {
            // Syntax check.
            args = argumentParserTPAccept.parse(args);
            if (args.length != 0 || argumentParserTPAccept.hasArgument('h')) {
                Messenger.send(sender, "commandHelp", "Syntax: /tpaccept");
                argumentParserTPAccept.showHelp(sender);
                return true;
            }

            // Find the requester.
            final UUID requester = teleportRequests.get(player.getUniqueId());
            final Player target = (requester == null) ? null : Bukkit.getPlayer(requester);
            if (requester == null || target == null || !target.isOnline()) {
                Messenger.send(sender, "rohrpost.tpa.noRequest");
                return true;
            }

            // Invalidate the request.
            teleportRequests.remove(player.getUniqueId());

            // Fetch the target location and teleport.
            final Location to = player.getLocation();
            final TeleportTask tp = new TeleportTask(target, to);
            tp.start();
            Messenger.send(target, "rohrpost.tpa.accepted", sender.getName());
        }

        return true;
    }
}
