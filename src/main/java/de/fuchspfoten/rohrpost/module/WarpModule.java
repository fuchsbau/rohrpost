package de.fuchspfoten.rohrpost.module;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.StringSerializer;
import de.fuchspfoten.rohrpost.RohrpostPlugin;
import de.fuchspfoten.rohrpost.TeleportTask;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * /warp &lt;name&gt; teleports to a warp.
 * /spawn teleports to /warp spawn.
 * /setwarp &lt;name&gt; sets a warp.
 */
public class WarpModule implements CommandExecutor {

    /**
     * The parser for command arguments for /spawn.
     */
    private final ArgumentParser argumentParserSpawn;

    /**
     * The parser for command arguments for /warp.
     */
    private final ArgumentParser argumentParserWarp;

    /**
     * The parser for command arguments for /sethome.
     */
    private final ArgumentParser argumentParserSetwarp;

    /**
     * The warp backend file.
     */
    private final File warpBackend;

    /**
     * The warp data storage.
     */
    private final FileConfiguration warpStorage;

    /**
     * Constructor.
     */
    public WarpModule(final File warpBackend) {
        this.warpBackend = warpBackend;
        warpStorage = YamlConfiguration.loadConfiguration(warpBackend);

        Messenger.register("rohrpost.warp.list");
        Messenger.register("rohrpost.warp.unknown");
        Messenger.register("rohrpost.warp.deleted");
        Messenger.register("rohrpost.warp.updated");

        argumentParserSpawn = new ArgumentParser();

        argumentParserWarp = new ArgumentParser();
        argumentParserWarp.addSwitch('l', "List warps");

        argumentParserSetwarp = new ArgumentParser();
        argumentParserSetwarp.addSwitch('x', "Delete the warp with the given name");

        Bukkit.getPluginCommand("warp").setExecutor(this);
        Bukkit.getPluginCommand("setwarp").setExecutor(this);
        Bukkit.getPluginCommand("spawn").setExecutor(this);
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        // Player check.
        if (!(sender instanceof Player)) {
            sender.sendMessage("player-only!");
            return true;
        }

        if (command.getName().equals("spawn")) {
            // Syntax check.
            args = argumentParserSpawn.parse(args);
            if (args.length != 0 || argumentParserSpawn.hasArgument('h')) {
                Messenger.send(sender, "commandHelp", "Syntax: /spawn");
                argumentParserSpawn.showHelp(sender);
                return true;
            }

            // Redirect the command.
            ((Player) sender).performCommand("warp spawn");
        } else if (command.getName().equals("warp")) {
            // Permission check.
            if (!sender.hasPermission("rohrpost.warp")) {
                sender.sendMessage("Missing permission: rohrpost.warp");
                return true;
            }

            // Syntax check.
            args = argumentParserWarp.parse(args);
            final boolean doesList = argumentParserWarp.hasArgument('l')
                    && sender.hasPermission("rohrpost.warp.list");
            if ((args.length != 1 && !doesList) || argumentParserWarp.hasArgument('h')) {
                Messenger.send(sender, "commandHelp", "Syntax: /warp <name>");
                argumentParserWarp.showHelp(sender);
                return true;
            }

            // Fetch the warps.
            final Map<String, Location> warps = warpStorage.getKeys(false).stream()
                    .collect(Collectors.toMap(x -> x, x -> StringSerializer.parseLocation(warpStorage.getString(x))));

            // List warps, if requested.
            if (doesList) {
                Messenger.send(sender, "rohrpost.warp.list", String.join(", ", warps.keySet()));
                return true;
            }

            // Fetch the requested warp.
            final String name = args[0];

            // Get the target location.
            final Location target = warps.get(name);
            if (target == null) {
                Messenger.send(sender, "rohrpost.warp.unknown");
                return true;
            }

            // Perform the teleport.
            final TeleportTask tp = new TeleportTask((Player) sender, target);
            tp.start();
        } else if (command.getName().equals("setwarp")) {
            // Permission check.
            if (!sender.hasPermission("rohrpost.warp.set")) {
                sender.sendMessage("Missing permission: rohrpost.warp.set");
                return true;
            }

            // Syntax check.
            args = argumentParserSetwarp.parse(args);
            if (args.length != 1 || argumentParserSetwarp.hasArgument('h')) {
                Messenger.send(sender, "commandHelp", "Syntax: /setwarp <name>");
                argumentParserSetwarp.showHelp(sender);
                return true;
            }

            // Fetch the warps.
            final Map<String, Location> warps = warpStorage.getKeys(false).stream()
                    .collect(Collectors.toMap(x -> x, x -> StringSerializer.parseLocation(warpStorage.getString(x))));

            // Fetch the requested warp.
            final String name = args[0];
            if (name.contains(".")) {
                sender.sendMessage("Invalid warp name");
                return true;
            }

            // Delete if requested.
            if (argumentParserSetwarp.hasArgument('x')) {
                if (!warps.containsKey(name)) {
                    Messenger.send(sender, "rohrpost.warp.unknown");
                    return true;
                }
                warpStorage.set(name, null);
                save();
                Messenger.send(sender, "rohrpost.warp.deleted");
                return true;
            }

            // Update the warp.
            warpStorage.set(name, StringSerializer.toString(((Entity) sender).getLocation()));
            save();
            Messenger.send(sender, "rohrpost.warp.updated");
        }

        return true;
    }

    /**
     * Saves warp data.
     */
    private void save() {
        try {
            warpStorage.save(warpBackend);
        } catch (final IOException e) {
            RohrpostPlugin.getSelf().getLogger().severe("Could not save warps.yml");
            e.printStackTrace();
        }
    }
}
