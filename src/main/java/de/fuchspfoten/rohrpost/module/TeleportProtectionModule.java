package de.fuchspfoten.rohrpost.module;

import de.fuchspfoten.rohrpost.RohrpostPlugin;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Protects on teleport.
 */
public class TeleportProtectionModule implements Listener {

    /**
     * Set of teleport-protected UUIDs.
     */
    private final Set<UUID> teleportProtection = new HashSet<>();

    /**
     * Set of UUIDs that are disqualified for join protection.
     */
    private final Set<UUID> joinProtectionDisqualified = new HashSet<>();

    @EventHandler
    public void onPlayerTeleport(final PlayerTeleportEvent event) {
        if (event.getCause() == TeleportCause.CHORUS_FRUIT
                || event.getCause() == TeleportCause.ENDER_PEARL) {
            // No protection on easy-to-achieve teleports.
            return;
        }

        final boolean protects = event.getFrom().getWorld() != event.getTo().getWorld()
                || event.getFrom().distanceSquared(event.getTo()) >= 4096;

        if (protects) {
            // Perform the protection.
            final UUID playerId = event.getPlayer().getUniqueId();
            teleportProtection.add(playerId);
            RohrpostPlugin.scheduleTask(() -> teleportProtection.remove(playerId), 7 * 20L);
        }
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        final UUID playerId = event.getPlayer().getUniqueId();
        if (joinProtectionDisqualified.contains(playerId)) {
            return;
        }

        // Perform the protection.
        teleportProtection.add(playerId);
        joinProtectionDisqualified.add(playerId);
        RohrpostPlugin.scheduleTask(() -> teleportProtection.remove(playerId), 10 * 20L);
        RohrpostPlugin.scheduleTask(() -> joinProtectionDisqualified.remove(playerId), 180 * 20L);
    }

    @EventHandler
    public void onEntityDamage(final EntityDamageEvent event) {
        if (!(event.getEntity() instanceof Player)) {
            return;
        }

        if (teleportProtection.contains(event.getEntity().getUniqueId())) {
            event.setCancelled(true);
        }
    }
}