package de.fuchspfoten.rohrpost.module;

import com.sk89q.worldedit.bukkit.selections.Selection;
import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.StringSerializer;
import de.fuchspfoten.fuchslib.data.DataFile;
import de.fuchspfoten.rohrpost.PortalStorageVersionUpdater;
import de.fuchspfoten.rohrpost.RohrpostPlugin;
import de.fuchspfoten.rohrpost.model.Portal;
import de.fuchspfoten.rohrpost.model.PortalLocation;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * /portal &lt;name&gt; manages the portal with the given name.
 */
public class PortalModule implements CommandExecutor, Listener {

    /**
     * The parser for command arguments for /portal.
     */
    private final ArgumentParser argumentParserPortal;

    /**
     * The portal storage.
     */
    private final DataFile portalStorage;

    /**
     * The currently registered portals.
     */
    private final Map<String, Portal> portals = new HashMap<>();

    /**
     * Maps portal locations to portals.
     */
    private final Map<PortalLocation, Portal> portalLocationMap = new HashMap<>();

    /**
     * Constructor.
     */
    public PortalModule(final File portalBackend) {
        portalStorage = new DataFile(portalBackend, new PortalStorageVersionUpdater());

        // Load all portals.
        for (final String portalName : portalStorage.getStorage().getKeys(false)) {
            if (!portalStorage.getStorage().isConfigurationSection(portalName)) {
                continue;
            }
            final ConfigurationSection section = portalStorage.getStorage().getConfigurationSection(portalName);
            final Portal portal = new Portal(portalName, StringSerializer.parseLocation(section.getString("min")),
                    StringSerializer.parseLocation(section.getString("max")));
            portal.setTargetLocation(StringSerializer.parseLocation(section.getString("target")));
            portals.put(portalName, portal);
        }
        recalculatePortals();

        argumentParserPortal = new ArgumentParser();
        argumentParserPortal.addSwitch('l', "List portals");
        argumentParserPortal.addSwitch('n', "Link portal to location");
        argumentParserPortal.addSwitch('x', "Delete Portal");

        Bukkit.getPluginCommand("portal").setExecutor(this);
    }

    @EventHandler
    public void onPlayerMove(final PlayerMoveEvent event) {
        final Block toBlock = event.getTo().getBlock();
        final PortalLocation toLoc = new PortalLocation(toBlock.getWorld().getName(), toBlock.getX(), toBlock.getY(),
                toBlock.getZ());
        final Portal portal = portalLocationMap.get(toLoc);
        if (portal != null) {
            // Stepped into a portal.
            RohrpostPlugin.scheduleTask(() -> event.getPlayer().teleport(portal.getTargetLocation()), 1L);
        }
    }

    /**
     * Recalculates the portal location map.
     */
    private void recalculatePortals() {
        portalLocationMap.clear();
        for (final Portal portal : portals.values()) {
            final int maxX = portal.getMaxLocation().getBlockX();
            for (int x = portal.getMinLocation().getBlockX(); x <= maxX; x++) {
                final int maxY = portal.getMaxLocation().getBlockY();
                for (int y = portal.getMinLocation().getBlockY(); y <= maxY; y++) {
                    final int maxZ = portal.getMaxLocation().getBlockZ();
                    for (int z = portal.getMinLocation().getBlockZ(); z <= maxZ; z++) {
                        final PortalLocation portalLocation =
                                new PortalLocation(portal.getMinLocation().getWorld().getName(), x, y, z);
                        portalLocationMap.put(portalLocation, portal);
                    }
                }
            }
        }
    }

    /**
     * Saves warp data.
     */
    private void save() {
        portalStorage.save(PortalStorageVersionUpdater.VERSION);
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        // Player check.
        if (!(sender instanceof Player)) {
            sender.sendMessage("player-only!");
            return true;
        }
        final Player player = (Player) sender;

        // Permission check.
        if (!player.hasPermission("rohrpost.manageportals")) {
            sender.sendMessage("Insufficient permission");
            return true;
        }

        args = argumentParserPortal.parse(args);
        final boolean argsOk = args.length == 1 || argumentParserPortal.hasArgument('l');
        if (!argsOk || argumentParserPortal.hasArgument('h')) {
            Messenger.send(sender, "commandHelp", "Syntax: /portal <name>");
            argumentParserPortal.showHelp(sender);
            return true;
        }

        if (argumentParserPortal.hasArgument('l')) {
            sender.sendMessage("Portals: " + String.join(", ",
                    portals.values().stream().map(Portal::getName).collect(Collectors.toList())));
            return true;
        }
        final String portalName = args[0].toLowerCase();

        if (argumentParserPortal.hasArgument('x') && portals.containsKey(portalName)) {
            portalStorage.getStorage().set(portalName, null);
            portals.remove(portalName);
            save();
            recalculatePortals();
            sender.sendMessage("Deleted");
            return true;
        }

        if (argumentParserPortal.hasArgument('n') && portals.containsKey(portalName)) {
            portalStorage.getStorage().getConfigurationSection(portalName).set("target",
                    StringSerializer.toString(player.getLocation()));
            portals.get(portalName).setTargetLocation(player.getLocation());
            save();
            sender.sendMessage("Linked");
            return true;
        }

        final Selection selection = RohrpostPlugin.getSelf().getWorldEdit().getSelection(player);
        if (selection == null) {
            sender.sendMessage("No selection");
            return true;
        }

        final Portal portal = new Portal(portalName, selection.getMinimumPoint(), selection.getMaximumPoint());
        portal.setTargetLocation(selection.getMinimumPoint());
        portals.put(portalName, portal);
        recalculatePortals();

        final ConfigurationSection sec = portalStorage.getStorage().createSection(portalName);
        sec.set("min", StringSerializer.toString(selection.getMinimumPoint()));
        sec.set("max", StringSerializer.toString(selection.getMaximumPoint()));
        sec.set("target", StringSerializer.toString(selection.getMinimumPoint()));
        save();
        sender.sendMessage("Portal outlines updated, link reset");

        return true;
    }
}
