package de.fuchspfoten.rohrpost.module;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.rohrpost.TeleportTask;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /goto &lt;world&gt; teleports to a world.
 */
public class GotoModule implements CommandExecutor {

    /**
     * The parser for command arguments for /goto.
     */
    private final ArgumentParser argumentParserGoto;

    /**
     * Constructor.
     */
    public GotoModule() {
        Messenger.register("rohrpost.goto.notExist");
        argumentParserGoto = new ArgumentParser();
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        // Player check.
        if (!(sender instanceof Player)) {
            sender.sendMessage("player-only!");
            return true;
        }
        final Player player = (Player) sender;

        // Permission check.
        if (!sender.hasPermission("rohrpost.goto")) {
            sender.sendMessage("Missing permission: rohrpost.goto");
            return true;
        }

        // Syntax check.
        args = argumentParserGoto.parse(args);
        if (args.length < 1 || argumentParserGoto.hasArgument('h')) {
            Messenger.send(sender, "commandHelp", "Syntax: /goto <world> [x y z]");
            argumentParserGoto.showHelp(sender);
            return true;
        }

        // Fetch the target.
        final World world = Bukkit.getWorld(args[0]);
        if (world == null) {
            Messenger.send(sender, "rohrpost.goto.notExist");
            return true;
        }

        final Location where;
        if (args.length == 4) {
            try {
                where = new Location(world, Integer.parseInt(args[1]), Integer.parseInt(args[2]),
                        Integer.parseInt(args[3]));
            } catch (final NumberFormatException ex) {
                return true;
            }
        } else {
            where = world.getSpawnLocation();
        }

        // Perform the teleport.
        final TeleportTask tp = new TeleportTask(player, where);
        tp.start();

        return true;
    }
}
