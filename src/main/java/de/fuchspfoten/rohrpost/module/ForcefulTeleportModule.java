package de.fuchspfoten.rohrpost.module;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.rohrpost.TeleportTask;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /tp &lt;player&gt; teleports to a player.
 * /s &lt;player&gt; summons a player.
 */
public class ForcefulTeleportModule implements CommandExecutor {

    /**
     * The parser for command arguments for /tp.
     */
    private final ArgumentParser argumentParserTP;

    /**
     * The parser for command arguments for /s.
     */
    private final ArgumentParser argumentParserSummon;

    /**
     * Constructor.
     */
    public ForcefulTeleportModule() {
        argumentParserTP = new ArgumentParser();
        argumentParserSummon = new ArgumentParser();

        Bukkit.getPluginCommand("tp").setExecutor(this);
        Bukkit.getPluginCommand("s").setExecutor(this);
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        // Player check.
        if (!(sender instanceof Player)) {
            sender.sendMessage("player-only!");
            return true;
        }
        final Player player = (Player) sender;

        if (command.getName().equals("tp")) {
            // Permission check.
            if (!sender.hasPermission("rohrpost.tp")) {
                sender.sendMessage("Missing permission: rohrpost.tp");
                return true;
            }

            // Syntax check.
            args = argumentParserTP.parse(args);
            if (args.length != 1 || argumentParserTP.hasArgument('h')) {
                Messenger.send(sender, "commandHelp", "Syntax: /tp <player>");
                argumentParserTP.showHelp(sender);
                return true;
            }

            // Fetch the target.
            final Player target = Bukkit.getPlayer(args[0]);
            if (target == null || !target.isOnline() || !player.canSee(target)) {
                sender.sendMessage("target not found");
                return true;
            }

            // Teleport to the target.
            final TeleportTask tp = new TeleportTask(player, target.getLocation());
            tp.start();
        } else if (command.getName().equals("s")) {
            // Permission check.
            if (!sender.hasPermission("rohrpost.summon")) {
                sender.sendMessage("Missing permission: rohrpost.summon");
                return true;
            }

            // Syntax check.
            args = argumentParserSummon.parse(args);
            if (args.length != 1 || argumentParserSummon.hasArgument('h')) {
                Messenger.send(sender, "commandHelp", "Syntax: /s <player>");
                argumentParserSummon.showHelp(sender);
                return true;
            }

            // Fetch the target.
            final Player target = Bukkit.getPlayer(args[0]);
            if (target == null || !target.isOnline() || !player.canSee(target)) {
                sender.sendMessage("target not found");
                return true;
            }

            // Teleport to the target.
            target.teleport(player);
        }

        return true;
    }
}
