package de.fuchspfoten.rohrpost.module;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.rohrpost.TeleportTask;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * /back returns to the previous location.
 */
public class BackModule implements CommandExecutor, Listener {

    /**
     * The parser for command arguments for /back.
     */
    private final ArgumentParser argumentParserBack;

    /**
     * Map of previous locations.
     */
    private final Map<UUID, Location> previousLocations = new HashMap<>();

    /**
     * Constructor.
     */
    public BackModule() {
        Messenger.register("rohrpost.back.nopos");

        argumentParserBack = new ArgumentParser();
        Bukkit.getPluginCommand("back").setExecutor(this);
    }

    @EventHandler
    public void onPlayerTeleport(final PlayerTeleportEvent event) {
        if (event.getFrom().getWorld() != event.getTo().getWorld()) {
            previousLocations.put(event.getPlayer().getUniqueId(), event.getFrom());
        } else if (event.getFrom().distanceSquared(event.getTo()) >= 256) {
            previousLocations.put(event.getPlayer().getUniqueId(), event.getFrom());
        }
    }

    @EventHandler
    public void onPlayerDeath(final PlayerDeathEvent event) {
        previousLocations.put(event.getEntity().getUniqueId(), event.getEntity().getLocation());
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        // Permission check.
        if (!sender.hasPermission("rohrpost.back")) {
            sender.sendMessage("Missing permission: rohrpost.back");
            return true;
        }

        // Player check.
        if (!(sender instanceof Player)) {
            sender.sendMessage("player-only!");
            return true;
        }

        // Syntax check.
        args = argumentParserBack.parse(args);
        if (args.length != 0 || argumentParserBack.hasArgument('h')) {
            Messenger.send(sender, "commandHelp", "Syntax: /back");
            argumentParserBack.showHelp(sender);
            return true;
        }

        // Return.
        final Location prev = previousLocations.get(((Entity) sender).getUniqueId());
        if (prev == null) {
            // Indicate the failure.
            Messenger.send(sender, "rohrpost.back.nopos");
        } else {
            // Return.
            final TeleportTask tp = new TeleportTask((Player) sender, prev);
            tp.start();
        }

        return true;
    }
}
