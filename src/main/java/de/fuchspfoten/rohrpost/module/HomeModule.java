package de.fuchspfoten.rohrpost.module;

import de.fuchspfoten.fuchslib.ArgumentParser;
import de.fuchspfoten.fuchslib.Messenger;
import de.fuchspfoten.fuchslib.StringSerializer;
import de.fuchspfoten.fuchslib.data.PlayerData;
import de.fuchspfoten.fuchslib.data.UUIDLookup;
import de.fuchspfoten.rohrpost.TeleportTask;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * /home [name] teleports to a home.
 * /sethome [name] sets a home.
 */
public class HomeModule implements CommandExecutor {

    /**
     * The pattern for home names.
     */
    private static final Pattern HOME_PATTERN = Pattern.compile("^[a-zA-Z0-9_]{1,32}$");

    /**
     * The parser for command arguments for /home.
     */
    private final ArgumentParser argumentParserHome;

    /**
     * The parser for command arguments for /sethome.
     */
    private final ArgumentParser argumentParserSethome;

    /**
     * The home limit without any special permission.
     */
    private final int regularHomeLimit;

    /**
     * The home limit with the rohrpost.home.multiple permission.
     */
    private final int multipleHomeLimit;

    /**
     * Constructor.
     */
    public HomeModule(final ConfigurationSection config) {
        Messenger.register("rohrpost.home.list");
        Messenger.register("rohrpost.home.unknown");
        Messenger.register("rohrpost.home.deleted");
        Messenger.register("rohrpost.home.updated");
        Messenger.register("rohrpost.home.needDelete");
        regularHomeLimit = config.getInt("home.regular");
        multipleHomeLimit = config.getInt("home.multiple");

        argumentParserHome = new ArgumentParser();
        argumentParserHome.addSwitch('l', "List homes");
        argumentParserHome.addArgument('u', "Perform the operation as the given user",
                "user");

        argumentParserSethome = new ArgumentParser();
        argumentParserSethome.addSwitch('x', "Delete the home with the given name");
        argumentParserSethome.addArgument('u', "Perform the operation as the given user",
                "user");

        Bukkit.getPluginCommand("home").setExecutor(this);
        Bukkit.getPluginCommand("sethome").setExecutor(this);
    }

    @Override
    public boolean onCommand(final CommandSender sender, final Command command, final String label, String[] args) {
        // Player check.
        if (!(sender instanceof Player)) {
            sender.sendMessage("player-only!");
            return true;
        }

        if (command.getName().equals("home")) {
            // Permission check.
            if (!sender.hasPermission("rohrpost.home")) {
                sender.sendMessage("Missing permission: rohrpost.home");
                return true;
            }

            // Syntax check.
            args = argumentParserHome.parse(args);
            if (args.length > 1 || argumentParserHome.hasArgument('h')) {
                Messenger.send(sender, "commandHelp", "Syntax: /home [name]");
                argumentParserHome.showHelp(sender);
                return true;
            }

            // Fetch the player data.
            final PlayerData data;
            if (argumentParserHome.hasArgument('u') && sender.hasPermission("rohrpost.home.sudo")) {
                data = new PlayerData(UUIDLookup.lookup(argumentParserHome.getArgument('u')));
            } else {
                data = new PlayerData((OfflinePlayer) sender);
            }

            // Fetch the homes.
            final ConfigurationSection homeSection = data.getStorage().getConfigurationSection("homes");
            final Map<String, Location> homes =
                    homeSection == null ? Collections.EMPTY_MAP : homeSection.getKeys(false).stream()
                            .collect(Collectors.toMap(x -> x,
                                    x -> StringSerializer.parseLocation(homeSection.getString(x))));

            // List homes, if requested.
            if (argumentParserHome.hasArgument('l')) {
                Messenger.send(sender, "rohrpost.home.list", String.join(", ", homes.keySet()));
                return true;
            }

            // Fetch the requested home.
            final String name;
            if (args.length == 1) {
                name = args[0];
            } else {
                name = "home";
            }

            // Get the target location.
            final Location target = homes.get(name);
            if (target == null) {
                Messenger.send(sender, "rohrpost.home.unknown");
                return true;
            }

            // Perform the teleport.
            final TeleportTask tp = new TeleportTask((Player) sender, target);
            tp.start();
        } else if (command.getName().equals("sethome")) {
            // Permission check.
            if (!sender.hasPermission("rohrpost.home")) {
                sender.sendMessage("Missing permission: rohrpost.home");
                return true;
            }

            // Syntax check.
            args = argumentParserSethome.parse(args);
            if (args.length > 1 || argumentParserSethome.hasArgument('h')) {
                Messenger.send(sender, "commandHelp", "Syntax: /sethome [name]");
                argumentParserSethome.showHelp(sender);
                return true;
            }

            // Fetch the player data.
            final PlayerData data;
            if (argumentParserSethome.hasArgument('u') && sender.hasPermission("rohrpost.home.sudo")) {
                data = new PlayerData(UUIDLookup.lookup(argumentParserSethome.getArgument('u')));
            } else {
                data = new PlayerData((OfflinePlayer) sender);
            }

            // Fetch the homes.
            final ConfigurationSection homeSection = data.getStorage().getConfigurationSection("homes");
            final Map<String, Location> homes =
                    homeSection == null ? Collections.EMPTY_MAP : homeSection.getKeys(false).stream()
                            .collect(Collectors.toMap(x -> x,
                                    x -> StringSerializer.parseLocation(homeSection.getString(x))));

            // Fetch the requested home.
            final String name;
            if (args.length == 1) {
                name = args[0];
            } else {
                name = "home";
            }
            if (!HOME_PATTERN.matcher(name).matches()) {
                sender.sendMessage("Invalid home name");
                return true;
            }

            // Delete if requested.
            if (argumentParserSethome.hasArgument('x')) {
                if (homeSection == null || !homes.containsKey(name)) {
                    Messenger.send(sender, "rohrpost.home.unknown");
                    return true;
                }
                homeSection.set(name, null);
                Messenger.send(sender, "rohrpost.home.deleted");
                return true;
            }

            // Check count.
            final int homeCount = homes.size();
            final int limit;
            if (sender.hasPermission("rohrpost.home.infinite")) {
                limit = Integer.MAX_VALUE;
            } else if (sender.hasPermission("rohrpost.home.multiple")) {
                limit = multipleHomeLimit;
            } else {
                limit = regularHomeLimit;
            }
            if (homeCount >= limit) {
                if (homes.containsKey(name)) {
                    // Overwrite home. Only deny if too many.
                    if (homeCount > limit) {
                        Messenger.send(sender, "rohrpost.home.needDelete");
                        return true;
                    }
                } else {
                    // New home. Deny.
                    Messenger.send(sender, "rohrpost.home.needDelete");
                    return true;
                }
            }

            // Update the home.
            data.getStorage().set("homes." + name, StringSerializer.toString(((Entity) sender).getLocation()));
            Messenger.send(sender, "rohrpost.home.updated");
        }

        return true;
    }
}
