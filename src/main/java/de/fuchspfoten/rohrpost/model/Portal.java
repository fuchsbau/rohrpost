package de.fuchspfoten.rohrpost.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.bukkit.Location;

/**
 * A portal.
 */
@RequiredArgsConstructor
@Getter
public class Portal {

    /**
     * The name of the portal.
     */
    private final String name;

    /**
     * The minimum location.
     */
    private final Location minLocation;

    /**
     * The maximum location.
     */
    private final Location maxLocation;

    /**
     * The target location.
     */
    private @Setter Location targetLocation;
}
