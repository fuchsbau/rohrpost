package de.fuchspfoten.rohrpost.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 * Represents a block coordinate triple.
 */
@RequiredArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class PortalLocation {

    /**
     * The world the portal is in.
     */
    private final String world;

    /**
     * The block X coordinate.
     */
    private final int x;

    /**
     * The block Y coordinate.
     */
    private final int y;

    /**
     * The block Z coordinate.
     */
    private final int z;
}
