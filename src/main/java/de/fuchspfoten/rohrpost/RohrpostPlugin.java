package de.fuchspfoten.rohrpost;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import de.fuchspfoten.rohrpost.module.BackModule;
import de.fuchspfoten.rohrpost.module.ForcefulTeleportModule;
import de.fuchspfoten.rohrpost.module.GotoModule;
import de.fuchspfoten.rohrpost.module.HomeModule;
import de.fuchspfoten.rohrpost.module.PortalModule;
import de.fuchspfoten.rohrpost.module.RequestTeleportModule;
import de.fuchspfoten.rohrpost.module.TeleportProtectionModule;
import de.fuchspfoten.rohrpost.module.WarpModule;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

/**
 * Plugin main class.
 */
public class RohrpostPlugin extends JavaPlugin {

    /**
     * The singleton plugin instance.
     */
    private @Getter static RohrpostPlugin self;

    /**
     * The worldedit plugin.
     */
    private @Getter WorldEditPlugin worldEdit;

    /**
     * Schedules a task with the scheduler.
     *
     * @param runnable The task.
     * @param ticks    How many ticks in the future the task will be executed.
     */
    public static void scheduleTask(final Runnable runnable, final long ticks) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(self, runnable, ticks);
    }

    @Override
    public void onEnable() {
        self = this;

        // Fetch the worldedit plugin.
        worldEdit = (WorldEditPlugin) getServer().getPluginManager().getPlugin("WorldEdit");

        // Create the default configuration.
        getConfig().options().copyDefaults(true);
        saveConfig();

        // Create the warp storage, if needed.
        final File warpFile = new File(getDataFolder(), "warps.yml");
        if (!warpFile.exists()) {
            try {
                if (!warpFile.createNewFile()) {
                    throw new IllegalStateException("can't create warps.yml: failure");
                }
            } catch (final IOException ex) {
                throw new IllegalStateException("can't create warps.yml", ex);
            }
        }

        // Read cooldown data from config and initialize teleport tasks.
        TeleportTask.registerFormats();
        TeleportTask.setTeleportTicks(getConfig().getInt("cooldown") * 20L);

        // Register command executor modules.
        getCommand("goto").setExecutor(new GotoModule());

        // Register listener modules.
        getServer().getPluginManager().registerEvents(new BackModule(), this);
        getServer().getPluginManager().registerEvents(new PortalModule(new File(getDataFolder(), "portals.yml")),
                this);
        getServer().getPluginManager().registerEvents(new TeleportProtectionModule(), this);

        // Explicitly register modules.
        //noinspection unused Registers itself on constructor call.
        final ForcefulTeleportModule ftModule = new ForcefulTeleportModule();
        //noinspection unused Registers itself on constructor call.
        final HomeModule hModule = new HomeModule(getConfig());
        //noinspection unused Registers itself on constructor call.
        final WarpModule warpModule = new WarpModule(warpFile);
        //noinspection unused Registers itself on constructor call.
        final RequestTeleportModule rtModule = new RequestTeleportModule();
    }
}
