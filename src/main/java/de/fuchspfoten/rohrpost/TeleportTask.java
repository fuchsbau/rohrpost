package de.fuchspfoten.rohrpost;

import de.fuchspfoten.fuchslib.Messenger;
import lombok.Setter;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * A task that will teleport a user.
 */
public class TeleportTask implements Runnable {

    /**
     * The amount of ticks before a teleport is performed.
     */
    private @Setter static long teleportTicks;

    /**
     * Registers used message formats.
     */
    public static void registerFormats() {
        Messenger.register("rohrpost.cooldown.issue");
        Messenger.register("rohrpost.cooldown.fail");
    }

    /**
     * The target player.
     */
    private final Player target;

    /**
     * The target location.
     */
    private final Location to;

    /**
     * The original location.
     */
    private Location compareLocation;

    /**
     * Constructor.
     *
     * @param target The target player.
     * @param to The target location.
     */
    public TeleportTask(final Player target, final Location to) {
        this.target = target;
        this.to = to;
    }

    /**
     * Starts the task.
     */
    public void start() {
        if (target.hasPermission("rohrpost.instant")) {
            target.teleport(to);
            return;
        }

        // Initiate the teleport in the future.
        compareLocation = target.getLocation();
        Messenger.send(target, "rohrpost.cooldown.issue");
        RohrpostPlugin.scheduleTask(this, teleportTicks);
    }

    @Override
    public void run() {
        if (target.getLocation().getWorld() == compareLocation.getWorld()) {
            if (target.getLocation().distanceSquared(compareLocation) < 1) {
                target.teleport(to);
                return;
            }
        }
        Messenger.send(target, "rohrpost.cooldown.fail");
    }
}
