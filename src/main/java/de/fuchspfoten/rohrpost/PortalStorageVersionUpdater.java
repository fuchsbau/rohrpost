package de.fuchspfoten.rohrpost;

import de.fuchspfoten.fuchslib.data.DataFile;

import java.util.function.BiConsumer;

/**
 * Keeps the data file version of the portal storage up to date.
 */
public class PortalStorageVersionUpdater implements BiConsumer<Integer, DataFile> {

    /**
     * The current version.
     */
    public static final int VERSION = 1;

    @Override
    public void accept(final Integer version, final DataFile dataFile) {
        // Do nothing.
    }
}
